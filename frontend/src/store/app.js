/* eslint-disable no-return-assign */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const url = 'http://158.46.92.146:8000'

const store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    data: [],
    deleted: []
  },
  getters: {
    data (state) {
      return state.data.filter(val => state.deleted.indexOf(val.code) === -1)
    },
    codes (state) {
      return state.data.map(val => val.code)
    },
    disabledCodes (state) {
      return state.deleted
    }
  },
  mutations: {
    updateAllCurrency (state, currencies) {
      Vue.set(state, 'data', currencies)
    },
    updateByCode (state, currency) {
      const newData = state.data.map(val => {
        if (val.code === currency.code) {
          return currency
        } else {
          return val
        }
      })

      Vue.set(state, 'data', newData)
    },
    deleteByCode (state, code) {
      if (state.deleted.indexOf(code) === -1) {
        state.deleted.push(code)
        Vue.set(state, 'deleted', state.deleted)
      }
    },
    addByCode (state, code) {
      Vue.set(state, 'deleted', state.deleted.filter(c => c !== code))
    }
  },
  actions: {
    updateAllCurrency (context) {
      axios.get(url).then(r => {
        context.commit('updateAllCurrency', r.data)
      }).catch(r => {
        console.error(r)
      })
    },
    updateByCode (context, payload) {
      axios.get(`${url}?code=${payload.charCode}`).then(r => {
        context.commit('updateByCode', r.data)
        payload.success()
      }).catch(r => {
        console.error(r)
      })
    }
  }
})

export default store
