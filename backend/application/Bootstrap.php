<?php

require_once 'Api/ApiManager.php';
require_once 'Api/Backend/CBRApiBackend.php';
require_once 'Api/Backend/ZendCache.php';
require_once 'Api/Backend/DBCache.php';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function _initREST()
    {
        $frontController = Zend_Controller_Front::getInstance();

        $frontController->setRequest(new REST_Request);
        $frontController->setResponse(new REST_Response);

        $restRoute = new Zend_Rest_Route($frontController, array(), array('api'));
        $frontController->getRouter()->addRoute('rest', $restRoute);
    }

    public function _initCache()
    {
        $cache = Zend_Cache::factory(
            'Core',
            'File',
            array(
                'lifetime' => 5, //cache is cleaned once a 5 minutes
                'automatic_serialization' => true
            ),
            array('cache_dir' => '../cache/')
        );

        Zend_Registry::set('Cache', $cache);
    }

    public function _initApi() {
        // DI hell =(
        $api = new ApiManager(new CBRApiBackend(new ZendCache(Zend_Registry::get('Cache'))));
        // $api = new ApiManager(new CBRApiBackend(new DBCache()));
        Zend_Registry::set('Api', $api);
    }
}

