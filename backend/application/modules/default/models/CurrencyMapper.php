<?php

class Default_Model_CurrencyMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;

        return $this;
    }

    public function getDbTable()
    {
        if ($this->_dbTable === null) {
            $this->setDbTable('Default_Model_DbTable_Currency');
        }

        return $this->_dbTable;
    }

    public function get($params)
    {
        $result = $this->getDbTable()->fetchRow($params);
        if (is_null($result)) {
            return null;
        }

        try {
            $currency = new Default_Model_Currency();
            $currency = $currency
                ->setId($result->id)
                ->setVerboseName($result->verbose_name)
                ->setCode($result->code)
                ->setCreated(new DateTime($result->created));
        }
        catch (Exception $e) {
            echo var_export($result);
        }

        return $currency;
    }

    public function save(Default_Model_Currency $currency)
    {
        $id = $currency->getId();
        $data = $currency->toArray();

        if (is_null($id)) {
            $data['created'] = date('Y-m-d H:i:s');
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array(
                'id = ?' => $id
            ));
        }
    }
}