<?php

class Default_Model_CurrencyDataMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;

        return $this;
    }

    public function getDbTable()
    {
        if ($this->_dbTable === null) {
            $this->setDbTable('Default_Model_DbTable_CurrencyData');
        }

        return $this->_dbTable;
    }

    public function getLastByCurrencyID($currencyID)
    {
        $result = $this->getDbTable()->fetchRow(array(
            'currency = ?' => $currencyID
        ));
         if (is_null($result)) {
            return null;
        }

        $currencyData = new Default_Model_CurrencyData();
        $currencyData = $currencyData
            ->setId($result->id)
            ->setCurrency($result->currency)
            ->setValue($result->value)
            ->setCreated($result->created);

        return $currencyData;
    }

    public function save(Default_Model_CurrencyData $currencyData)
    {
        $id = $currencyData->getId();

        $data = $currencyData->toArray();

        if (is_null($id)) {
            $data['created'] = date('Y-m-d H:i:s');
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array(
                'id = ?' => $id
            ));
        }
    }
}