<?php

class Default_Model_Currency
{
    protected $_id;
    protected $_verboseName;
    protected $_code;
    protected $_created;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid currency property');
        }

        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid currency property');
        }

        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);

            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getVerboseName()
    {
        return $this->_verboseName;
    }

    public function setVerboseName($verboseName)
    {
        $this->_verboseName = (string) $verboseName;
        return $this;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function setCode($code)
    {
        $this->_code = (string) $code;
        return $this;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function setCreated($created)
    {
        $this->_created = $created;
        return $this;
    }

    public function toArray()
    {
        return array(
            'verbose_name' => $this->_verboseName,
            'code' => $this->_code,
            'created' => $this->_created
        );
    }
}