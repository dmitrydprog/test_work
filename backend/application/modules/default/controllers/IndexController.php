<?php

class Default_IndexController extends Zend_Rest_Controller
{
    public function headAction()
    {
    }

    public function getAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }

    public function indexAction()
    {
        $api = Zend_Registry::get('Api');
        $currencyCode = $this->getParam('code');

        if (is_null($currencyCode))
        {
            $responseData = $api->getApiBackEnd()->getAllCurrency();
        }
        else
        {
            $responseData = $api->getApiBackEnd()->getValueByCode($currencyCode);
        }

        $this->_helper->json($responseData);
    }
}

