<?php

require_once "Api/Currency/Currency.php";

/**
 * Base class for currency.
 * Inherit new currencies from this class.
 */
abstract class BaseCurrency implements Currency
{
    private $source;
    private $verboseName;

    /**
     * BaseCurrency constructor.
     * @param IValueSource $source - Source of data.
     * @param $verboseName - Human readable currency name.
     */
    public function __construct(IValueSource $source, $verboseName)
    {
        $this->source = $source;
        $this->verboseName = $verboseName;
    }

    public function getVerboseName()
    {
        return $this->verboseName;
    }

    public function getValue()
    {
        $value = $this->source->getValue();
        return $value;
    }
}