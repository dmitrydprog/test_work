<?php

interface Currency
{

    /**
     * Get human readable currency name.
     * @return mixed - Name.
     */
    public function getVerboseName();


    /**
     * Get current value of currency.
     * @return float - The value of the currency.
     */
    public function getValue();
}