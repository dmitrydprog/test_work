<?php

require_once "Api/Sources/ISource.php";
require_once "Api/Currency/BaseCurrency.php";

class EUR extends BaseCurrency
{
    public function __construct(IValueSource $source)
    {
        parent::__construct($source, "Евро");
    }
}
