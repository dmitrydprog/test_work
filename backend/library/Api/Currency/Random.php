<?php

require_once "Api/Sources/ISource.php";
require_once "Api/Currency/BaseCurrency.php";

/**
 * Mock random currency, use for tests.
 */
class Random extends BaseCurrency
{
    public function __construct(IValueSource $source)
    {
        parent::__construct($source, 'Random');
    }
}
