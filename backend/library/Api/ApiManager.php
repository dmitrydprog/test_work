<?php

require_once 'Api/Backend/ApiBackend.php';

class ApiManager
{
    private $_apiBackEnd;

    /**
     * ApiManager constructor.
     * @param ApiBackend $backend.
     */
    public function __construct(ApiBackend $backend)
    {
        $this->_apiBackEnd = $backend;
    }

    /**
     * @return ApiBackend - Current api backend.
     */
    public function getApiBackEnd()
    {
        return $this->_apiBackEnd;
    }
}