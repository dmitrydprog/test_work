<?php

require_once 'Api/Sources/ISource.php';


/**
 * Class CBRBaseSource
 * Based class for central bank source.
 */
abstract class CBRBaseSource implements IValueSource
{
    /**
     * CharCode currency, same as in api CBR.
     */
    protected $charCode;

    /**
     * Parsed xml file and extract needed currency value.
     * Work with http://www.cbr.ru.
     * @param $response_data - XML data.
     * @return string - Currency value on current day.
     */
    private function getValueFromData($response_data)
    {
        $xmlRoot = new SimpleXMLElement($response_data);

        foreach ($xmlRoot->Valute as $curs) {
            if ($curs->CharCode == $this->charCode) {
                return (string)$curs->Value;
            }
        }

        return null;
    }

    /**
     * Get currency value on current day.
     * @return string - Value.
     */
    public function getValue()
    {
        $response_data = file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp");
        return $this->getValueFromData($response_data);
    }
}