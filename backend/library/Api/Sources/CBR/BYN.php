<?php

require_once "Api/Sources/CBR/CBRBaseSource.php";

/**
 * SBR resource for Belarusian Ruble.
 */
class CBRBYNSource extends CBRBaseSource
{
    protected $charCode = 'BYN';
}