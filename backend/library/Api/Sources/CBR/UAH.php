<?php

require_once "Api/Sources/CBR/CBRBaseSource.php";

/**
 * SBR resource for Ukrainian Hryvnia.
 */
class CBRUAHSource extends CBRBaseSource
{
    protected $charCode = 'UAH';
}