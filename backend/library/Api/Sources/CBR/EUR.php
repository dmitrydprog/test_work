<?php

require_once "Api/Sources/CBR/CBRBaseSource.php";

/**
 * SBR resource for Euro.
 */
class CBREURSource extends CBRBaseSource
{
    protected $charCode = 'EUR';
}