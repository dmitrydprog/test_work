<?php

require_once "Api/Sources/CBR/CBRBaseSource.php";

/**
 * SBR resource for USD.
 */
class CBRUSDSource extends CBRBaseSource
{
    protected $charCode = 'USD';
}