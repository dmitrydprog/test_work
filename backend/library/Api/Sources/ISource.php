<?php

interface IValueSource
{
    /**
     * Get currency value from specified source.
     * @return float - currency value.
     */
    function getValue();
}