<?php

require_once "Api/Sources/CBR/CBRBaseSource.php";

/**
 * Mock resource, use for tests.
 * Return random currency value on per request.
 */
class RandomSource implements IValueSource
{
    public function getValue()
    {
        return mt_rand(1, 100);
    }
}