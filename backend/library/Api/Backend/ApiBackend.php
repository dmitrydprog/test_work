<?php

interface ApiBackend
{
    /**
     * Get currency value by code.
     * @param $code - Currency code.
     * @return float - The value of the currency.
     */
    public function getValueByCode($code);


    /**
     * Get list all registered currencies.
     * @return Currency[] - Registered currencies.
     */
    public function getAllCurrency();
}