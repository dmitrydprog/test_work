<?php

require_once 'Api/Backend/ApiBackend.php';

/**
 * Base api backend.
 * All other api backend must inherit this class.
 */
abstract class BaseApiBackend implements ApiBackend
{
    private $_currencyList;

    /**
     * Register new currency of code.
     * @param $code - Currency code.
     * @param Currency $currency - Specified currency.
     */
    protected function addCurrency($code, Currency $currency)
    {
        $this->_currencyList[$code] = $currency;
    }

    public function getValueByCode($code)
    {
         $currency = $this->_currencyList[$code];

         return array(
             'verbose_name' => $currency->getVerboseName(),
             'value' => $currency->getValue(),
             'code' => $code
         );
    }

    public function getAllCurrency()
    {
        return array_map(function($code){
            return $this->getValueByCode($code);
        }, array_keys($this->_currencyList));
    }
}