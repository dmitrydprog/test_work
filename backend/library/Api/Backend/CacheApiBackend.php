<?php

require_once 'Api/Backend/BaseApiBackend.php';
require_once 'Api/Backend/Cacheable.php';

/**
 * Cache api backend.
 * Caches the value of the currency, using code as the key for cache.
 */
class CacheApiBackend extends BaseApiBackend
{
    protected $_cache;

    /**
     * CacheApiBackend constructor.
     * @param Cacheable $cache - Instant Cacheable.
     */
    public function __construct(Cacheable $cache)
    {
        $this->_cache = $cache;
    }

    public function getValueByCode($code)
    {
        if (!$value = $this->_cache->load($code))
        {
            $value = parent::getValueByCode($code);
            $this->_cache->save($value, $code);
        }

        return $value;
    }
}