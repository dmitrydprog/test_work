<?php

require_once 'Api/Backend/Cacheable.php';

class ZendCache implements Cacheable
{
    protected $_cache;

    public function __construct(Zend_Cache_Core $cache)
    {
        $this->_cache = $cache;
    }

    public function load($key)
    {
        return $this->_cache->load($key);
    }

    public function save($key, $value)
    {
        return $this->_cache->save($key, $value);
    }
}