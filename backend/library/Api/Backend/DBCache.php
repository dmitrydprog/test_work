<?php

require_once 'Api/Backend/Cacheable.php';

define('expiredTime', (60 * 60 * 24));

class DBCache implements Cacheable
{
    public function load($key)
    {
        $currencyMapper = new Default_Model_CurrencyMapper();
        $currency = $currencyMapper->get(array(
            'code = ?' => $key
        ));

        if (is_null($currency)) {
            return false;
        }

        $currencyDataMapper = new Default_Model_CurrencyDataMapper();
        $currencyData = $currencyDataMapper->getLastByCurrencyID($currency->getId());

        if (is_null($currencyData)) {
            return false;
        }

        $createdDataTimestamp = (new DateTime($currencyData->getCreated()))->getTimestamp();
        $nowTimestamp = (new DateTime())->getTimestamp();

        if ($nowTimestamp - $createdDataTimestamp > expiredTime) {
            return false;
        }

        return array(
            'verbose_name' => $currency->getVerboseName(),
            'code' => $currency->getCode(),
            'value' => $currencyData->getValue()
        );
    }

    public function save($value, $key)
    {
        $currencyMapper = new Default_Model_CurrencyMapper();
        $currency = $currencyMapper->get(array(
            'code = ?' => $key
        ));

        if (is_null($currency)) {
            $currencyMapper->save(new Default_Model_Currency(array(
                'verboseName' => $value['verbose_name'],
                'code' => $value['code']
            )));
        }

        $currency = $currencyMapper->get(array(
            'code = ?' => $key
        ));

        $currencyDataMapper = new Default_Model_CurrencyDataMapper();
        $currencyData = $currencyDataMapper->getLastByCurrencyID($currency->getId());

        if (is_null($currencyData)) {
            $currencyDataMapper->save(new Default_Model_CurrencyData(array(
                'currency' => $currency->getId(),
                'value' => $value['value']
            )));
        }
    }
}