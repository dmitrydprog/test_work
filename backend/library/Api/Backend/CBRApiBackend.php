<?php

require_once 'Api/Backend/CacheApiBackend.php';
require_once 'Api/Backend/Cacheable.php';

require_once 'Api/Currency/BYN.php';
require_once 'Api/Currency/USD.php';
require_once 'Api/Currency/EUR.php';
require_once 'Api/Currency/UAH.php';
require_once 'Api/Currency/Random.php';

require_once 'Api/Sources/CBR/BYN.php';
require_once 'Api/Sources/CBR/USD.php';
require_once 'Api/Sources/CBR/EUR.php';
require_once 'Api/Sources/CBR/UAH.php';
require_once 'Api/Sources/Random.php';

/**
 * Central bank api backend.
 * By default registers all central bank currency.
 */
class CBRApiBackend extends CacheApiBackend
{
    public function __construct(Cacheable $cache)
    {
        parent::addCurrency('byn', new BYN(new CBRBYNSource()));
        parent::addCurrency('usd', new USD(new CBRUSDSource()));
        parent::addCurrency('eur', new EUR(new CBREURSource()));
        parent::addCurrency('uah', new UAH(new CBRUAHSource()));
        parent::addCurrency('rnd', new Random(new RandomSource()));
        parent::__construct($cache);
    }
}