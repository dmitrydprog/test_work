<?php

interface Cacheable {
    public function load($key);
    public function save($key, $value);
}