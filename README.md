# Setup guide

## Frontend build

Clone this repository.
```bash
git clone https://bitbucket.org/dmitrydprog/test_work.git repo
```

Change directory on `frontend`.
```bash
cd ./repo/frontend
```
 
Install all dependencies for build. 
``` bash
# install dependencies
npm install

# build for production with minification
npm run build
```

## Install docker with get-docker script.
```bash
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh
```

## Install docker-compose

Using script.
```bash
curl -L https://github.com/docker/compose/releases/download/1.14.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

Or install using pip.
```bash
pip install docker-compose
```

## Start docker compose
Change directory to `repo`.
Start docker compose.
```bash
docker-compose up -d
```

Visit `http://localhost` to access the frontend project.
And also `http://localhost:8000` to access the API.